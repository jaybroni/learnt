//
//  ProfileViewController.swift
//  Learnt
//
//  Created by jay on 2/14/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

let offset_HeaderStop:CGFloat = 120.0 // At this offset the Header stops its transformations
let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label

class ProfileViewController: UIViewController, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate{

    var profileJson: [AnyObject] = [[
        "firstName":"Jay",
        "lastName":"Buangan",
        "username":"jaybroni",
        "followersQty":"123",
        "followingQty":"2312",
        "iconFile":"profileIcon.png"
        ]]
    var dataJson: [AnyObject] = [[
        "userId": "1",
        "username":"jaybroni",
        "icon":"Tiger-Woods_7.jpg",
        "body":"I just realized that my life long dream was to be a pilot and not a fighter",
        "source":"wikipedia.com",
        "likes": "25",
        "comments": "30",
        "favorites": "10",
        "postDate":"1h",
        "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "2",
            "username":"joya",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Fiji water company ran the slogan \"Fiji, because it's not bottled in Cleveland\". The Cleveland water dept. Took offense, and ran a test determining that Fiji water has more impurities than the cities' tap water.",
            "source":"yahoo.com",
            "likes": "40",
            "comments": "10",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "3",
            "username":"arlor",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Ema!  Look at me!  I'm a mermaid!",
            "source":"disney.com",
            "likes": "1",
            "comments": "4",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"japan.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"richard.buangan.net",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"aria.buangan.net",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"emiliana.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"leartapp.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"naturewise.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"sony.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"appsilog.co",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"whoyoube.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"tahitianfairies.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"connick.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"averylonglonglonglongdomain.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"icanhazcheesburger.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"playboy.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"collegeclub.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"people.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"yahoo.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"facebook.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg"
        ]
    ]
    var learntItems: NSMutableArray = []
    var userItems: NSMutableArray = []
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var profileIcon: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var viewInScrollView: UIView!
    
    @IBOutlet weak var fullName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //scrollView.delegate = self
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.estimatedRowHeight = 227.0
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.learntItems = NSMutableArray(array: dataJson)
        self.tableView!.reloadData()
        self.userItems = NSMutableArray(array: profileJson)
        let firstName: AnyObject = self.userItems[0]["firstName"]!!
        let lastName: AnyObject = self.userItems[0]["lastName"]!!
        let userName:AnyObject = self.userItems[0]["username"]!!
        let followersQty:AnyObject = self.userItems[0]["followersQty"]!!
        let followingQty: AnyObject = self.userItems[0]["followingQty"]!!
        
        self.fullName.text = "\(firstName) \(lastName)"
        self.username.text = "@\(userName)"
        self.followerCount.text = "\(followersQty)"
        self.followingCount.text = "\(followingQty)"
        self.profileIcon.image = UIImage(named: self.userItems[0]["iconFile"] as String)
        // Do any additional setup after loading the view.
        var tableViewHeader = CGRect(x:0,y: 0, width: self.tableView.frame.size.width, height:self.header.frame.size.height )
        self.tableView.tableHeaderView?.frame = tableViewHeader
        var currentWindow = UIApplication.sharedApplication().keyWindow
       
        
        
        
       
       // currentWindow?.addSubview(header)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        var offset = scrollView.contentOffset.y
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        if offset > 0 {
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
       
           
            
            
        }
         var contentOffset:CGPoint = CGPointMake(0.0, min(offset + 64.0, 44.0));
        //tableView.contentOffset = contentOffset
       // header.layer.transform = headerTransform
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showFeed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.learntItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as LearntTableViewCell
        
        
        // Configure the cell...
        cell.username!.text = self.learntItems[indexPath.row]["username"] as NSString
        cell.profileIcon!.image = UIImage(named: self.learntItems[indexPath.row]["icon"] as NSString)
        cell.body!.text = self.learntItems[indexPath.row]["body"] as NSString
        cell.likesQty!.text = self.learntItems[indexPath.row]["likes"] as NSString
        cell.commentsQty!.text = self.learntItems[indexPath.row]["comments"] as NSString
        cell.source!.text = self.learntItems[indexPath.row]["source"] as NSString
        cell.bodyBackground!.image = UIImage(named: self.learntItems[indexPath.row]["backgroundImage"] as NSString)
        //cell.navController = self.navigationController
        
        let gotoProfileImageTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
        let gotoProfileTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
        let bodyTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoBody:")
        let likesTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "likeIt:")
        let commentTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addComment:")
       
        
        cell.profileIcon!.addGestureRecognizer(gotoProfileImageTap)
        cell.username!.addGestureRecognizer(gotoProfileTap)
        cell.body!.addGestureRecognizer(bodyTap)
        cell.likesBtn.addGestureRecognizer(likesTap)
        cell.commentsBtn.addGestureRecognizer(commentTap)
       
        return cell
    }
    
    func gotoProfile(recognizer: UIGestureRecognizer){
              
        
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
           
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileViewController") as ProfileViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func gotoBody(recognizer: UIGestureRecognizer){
        
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
            let username = self.learntItems[indexPath.row]["username"] as NSString
            let profileIcon = self.learntItems[indexPath.row]["icon"] as NSString
            let body = self.learntItems[indexPath.row]["body"] as NSString
            let source = self.learntItems[indexPath.row]["source"] as NSString
            let bodyBackground = self.learntItems[indexPath.row]["backgroundImage"] as NSString
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BodyViewController") as BodyViewController
            vc.usernameData = username
            vc.profileIconData = profileIcon
            vc.bodyTextData = body
            vc.sourceData = source
            vc.bodyBackgroundData = bodyBackground
            
            self.navigationController?.pushViewController(vc, animated: true)

           
        }
     
    }
    
    func likeIt(recognizer: UIGestureRecognizer){
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
            var likesQty = self.learntItems[indexPath.row]["likes"] as String
            var cell = self.tableView.cellForRowAtIndexPath(indexPath) as LearntTableViewCell
            
            if (cell.userLiked){
                var newLikesQty: Int = likesQty.toInt()! - 1
                
                //  self.learntItems[indexPath.row]["likes"] = "12"
                
                
                cell.likesQty!.text = String(newLikesQty)
                cell.userLiked = false
                cell.likesBtn.image = UIImage(named: "like_hollow.png")
            }else{
                var newLikesQty: Int = likesQty.toInt()! + 1
                cell.likesQty!.text = String(newLikesQty)
                cell.userLiked = true
                cell.likesBtn.image = UIImage(named: "like_filled.png")
            }
            
            
        }
        
        
    }
    
    func addComment(recognizer: UIGestureRecognizer){
        let cell = recognizer.view as LearntTableViewCell
        println("comment it: \(cell.commentsQty!.text)")
    }
    
  
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
