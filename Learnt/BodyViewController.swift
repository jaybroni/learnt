//
//  BodyViewController.swift
//  Learnt
//
//  Created by jay on 2/20/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit
import Social

extension UIView {
    
    func pb_takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.mainScreen().scale);
        
        self.drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        
        // old style: self.layer.renderInContext(UIGraphicsGetCurrentContext())
        
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
    }
}

class BodyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate {

    var commentsDataJSON:[AnyObject] = [[
        "profileIcon":"profileIcon.png",
        "firstName":"Jay",
        "lastName":"Buangan",
        "postDate":"15m",
        "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ],
        [
            "profileIcon":"profileIcon.png",
            "firstName":"Jay",
            "lastName":"Buangan",
            "postDate":"15m",
            "comment":"This is something that I didn't know. I wish I did because this taught me a lot.  There once was a man who told me the difference between right and wrong.  After reading this post I realized he was wrong and i was right.  He needs to take a shower."
        ]]

    @IBOutlet weak var profileIcon: UIImageView!
 
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var bodyText: UILabel!
    @IBOutlet weak var source: UILabel!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var likeButton: UIView!
    @IBOutlet weak var commentsButton: UIView!
    @IBOutlet weak var shareButton: UIView!
    @IBOutlet weak var bodyBackgoundImage: UIImageView!
    @IBOutlet weak var mergedView: UIView!
    
    var commentsData:NSMutableArray = []
    
    var profileIconData: String!
    var fullNameData: String!
    var usernameData: String!
    var bodyTextData: String!
    var sourceData: String!
    var bodyBackgroundData: String!
    
    var footerView: UIView!
    var commentBox: UITextField!
    var commentSubmit: UIButton!
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentsTableView.estimatedRowHeight = 20.0
        self.commentsTableView.rowHeight = UITableViewAutomaticDimension;
        
        self.commentsData = NSMutableArray(array: commentsDataJSON)
        self.profileIcon.image = UIImage(named: profileIconData)
        self.bodyBackgoundImage.image = UIImage(named: bodyBackgroundData)
        self.username.text = "\(usernameData)"
        self.bodyText.text = bodyTextData
        self.source.text = sourceData
        self.commentsTableView.dataSource = self
        self.commentsTableView.delegate = self
        self.bodyText.lineBreakMode = .ByWordWrapping
        self.bodyText.sizeToFit()
        self.likeButton.layer.borderColor = UIColorFromRGB(0xD8D8D8).CGColor
        self.commentsButton.layer.borderColor = UIColorFromRGB(0xD8D8D8).CGColor
        self.shareButton.layer.borderColor = UIColorFromRGB(0xD8D8D8).CGColor
        
        var commentTapGesture = UITapGestureRecognizer(target: self, action: "displayComments")
        self.commentsButton.addGestureRecognizer(commentTapGesture)
        
        var shareTapGesture = UITapGestureRecognizer(target:self, action: "share")
        self.shareButton.addGestureRecognizer(shareTapGesture)
        
        /* Setup the keyboard notifications */
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyBoardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyBoardWillHide:", name: UIKeyboardWillHideNotification, object: nil)

        self.commentsTableView.contentInset = UIEdgeInsetsZero
        self.commentsTableView.scrollIndicatorInsets = UIEdgeInsetsZero
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    
    func share(){
        let optionMenu = UIAlertController(title: nil, message: "Share Options", preferredStyle: .ActionSheet)
        
        let facebookShare = UIAlertAction(title: "Facebook", style: .Default, handler:{
            (alert: UIAlertAction!) -> Void in
            
            self.showShareFacebook()
        })

        let twitterShare = UIAlertAction(title: "Twitter", style: .Default, handler:{
            (alert: UIAlertAction!) -> Void in
            
            self.showShareTwitter()
        })
        
        let copyLinkShare = UIAlertAction(title: "Copy Link", style: .Default, handler:{
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        let cancelActionSheet = UIAlertAction(title: "Cancel", style: .Cancel, handler:{
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(facebookShare)
        optionMenu.addAction(twitterShare)
        optionMenu.addAction(copyLinkShare)
        optionMenu.addAction(cancelActionSheet)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)


        
    }
    
    func showShareFacebook(){
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            var fbSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            
            fbSheet.setInitialText("Look what i learned today!")
            fbSheet.addImage(mergedView.pb_takeSnapshot())
            self.presentViewController(fbSheet, animated: true, completion: nil)
        }
    }
    
    func showShareTwitter(){
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            var tweetSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            tweetSheet.setInitialText("Look what i learned today!")
            tweetSheet.addImage(mergedView.pb_takeSnapshot())
            
            self.presentViewController(tweetSheet, animated: true, completion: nil)
        }
    }
    
    func displayComments(){
        self.commentBox.becomeFirstResponder()
        self.footerView.hidden = false
        
        

    }
    func keyBoardWillShow(notification: NSNotification) {
        var info:NSDictionary = notification.userInfo!
        var keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as NSValue).CGRectValue().size
        
        var keyboardHeight:CGFloat =  keyboardSize.height
        
        var animationDuration:CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as CGFloat
        
        var contentInsets: UIEdgeInsets = UIEdgeInsetsMake(self.navigationController!.navigationBar.frame.size.height, 0.0, keyboardHeight, 0.0);
        self.commentsTableView.contentInset = contentInsets
        self.commentsTableView.scrollIndicatorInsets = contentInsets
     
        let indexPath = NSIndexPath(forRow: self.commentsData.count - 10, inSection: 0)
        self.commentsTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
        
    }
    
    func keyBoardWillHide(notification: NSNotification) {
        
        self.commentsTableView.contentInset = UIEdgeInsetsZero
        self.commentsTableView.scrollIndicatorInsets = UIEdgeInsetsZero
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: UITableViewData
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.commentsData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = commentsTableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as CommentTableViewCell
        
        let firstName = self.commentsData[indexPath.row]["firstName"] as NSString
        let lastName = self.commentsData[indexPath.row]["lastName"] as NSString
        cell.profileIcon!.image = UIImage(named: self.commentsData[indexPath.row]["profileIcon"] as NSString)
        cell.fullName!.text = "\(firstName) \(lastName)"
        cell.comment!.text = self.commentsData[indexPath.row]["comment"] as NSString
        cell.timeAgo!.text = self.commentsData[indexPath.row]["postDate"] as NSString
        
        let gotoProfileImageTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
        let gotoProfileTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
       
        cell.profileIcon!.addGestureRecognizer(gotoProfileImageTap)
        cell.fullName!.addGestureRecognizer(gotoProfileTap)
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        //footerView = UIView(frame: CGRect(x: 0, y: 50, width: tableView.bounds.width, height: 50))
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 500))
        footerView?.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1)
        
        
        commentBox = UITextField(frame: CGRect(x: 10, y: 5, width: tableView.bounds.width - 100 , height: 40))
        commentBox?.backgroundColor = UIColor.whiteColor()
        commentBox?.layer.cornerRadius = 2
        commentBox?.layer.borderWidth = 1
        commentBox?.layer.borderColor = UIColorFromRGB(0xD8D8D8).CGColor
        footerView?.addSubview(commentBox!)
        
        commentSubmit = UIButton(frame: CGRect(x: tableView.bounds.width - 85, y: 5, width: 80 , height: 40))
        commentSubmit.setTitle("Comment", forState: UIControlState.Normal)
        commentSubmit.backgroundColor = UIColorFromRGB(0xBBBBBB)
        commentSubmit.layer.cornerRadius = 5
        commentSubmit.addTarget(self, action: "comment:", forControlEvents: UIControlEvents.TouchUpInside)
        footerView?.addSubview(commentSubmit)
        commentBox?.delegate = self
        
        

        return footerView
    }
    
    func comment(sender:AnyObject){
            }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.footerView != nil {
            return self.footerView!.bounds.height
        }
        return 50
    }
    
    func gotoProfile(recognizer: UIGestureRecognizer){
        
        println("gotoProfile")
        var point = recognizer.locationInView(self.commentsTableView)
        if let indexPath = self.commentsTableView.indexPathForRowAtPoint(point)
        {
            
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileViewController") as ProfileViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func mergeImages(image: UIImage, text: UILabel) -> UIImage
    {
        UIGraphicsBeginImageContext(image.size)
        
        image.drawInRect(CGRectMake(0 , 0, image.size.width, image.size.height))
        
        text.drawTextInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        
        var mergedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return mergedImage
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
