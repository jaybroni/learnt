//
//  LearntTableViewController.swift
//  Learnt
//
//  Created by jay on 2/12/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class LearntTableViewController: UITableViewController {
    
    @IBAction func compose(sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let composeVC: ComposeViewController = storyboard.instantiateViewControllerWithIdentifier("compose") as ComposeViewController
        composeVC.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(composeVC, animated: true, completion: nil)
        /*
let alert = SCLAlertView()
        alert.addButton("Login", target:self, selector:Selector("firstButton"))
        alert.addButton("Sign up") {
            println("Second button tapped")
        }
        alert.showWarning("Please log in", subTitle: "You can't post anything without an account", closeButtonTitle: "Close", duration: 0)
*/
        
    }
    var dataJson: [AnyObject] = [[
        "userId": "1",
        "username":"jaybroni",
        "icon":"Tiger-Woods_7.jpg",
        "body":"I just realized that my life long dream was to be a pilot and not a fighter",
        "source":"wikipedia.com",
        "likes": "25",
        "comments": "30",
        "favorites": "10",
        "postDate":"1h",
        "backgroundImage":"image4.jpg",
        "userLiked":"false"
        ],
        [
            "userId": "2",
            "username":"joya",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Fiji water company ran the slogan \"Fiji, because it's not bottled in Cleveland\". The Cleveland water dept. Took offense, and ran a test determining that Fiji water has more impurities than the cities' tap water.",
            "source":"yahoo.com",
            "likes": "40",
            "comments": "10",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"true"
        ],
        [
            "userId": "3",
            "username":"arlor",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Ema!  Look at me!  I'm a mermaid!",
            "source":"disney.com",
            "likes": "1",
            "comments": "4",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
            
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"true"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"japan.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"richard.buangan.net",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"aria.buangan.net",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"emiliana.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"leartapp.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"naturewise.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"sony.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"appsilog.co",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"----",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"whoyoube.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"tahitianfairies.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"connick.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"averylonglonglonglongdomain.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"icanhazcheesburger.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"playboy.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"collegeclub.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image3.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"people.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"yahoo.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image1.jpg",
            "userLiked":"false"
        ],
        [
            "userId": "4",
            "username":"roguepinoy",
            "icon":"Tiger-Woods_7.jpg",
            "body":"Still trying to figure this out.  test, test, test, test.",
            "source":"facebook.com",
            "likes": "412",
            "comments": "197",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image2.jpg",
            "userLiked":"false"
        ]
    ]
    
    
    var learntItems: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.estimatedRowHeight = 20.0
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.learntItems = NSMutableArray(array: dataJson)
        self.tableView!.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.learntItems.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as LearntTableViewCell
        
        // Configure the cell...
        cell.username!.text = self.learntItems[indexPath.row]["username"] as NSString
        cell.profileIcon!.image = UIImage(named: self.learntItems[indexPath.row]["icon"] as NSString)
        cell.body!.text = self.learntItems[indexPath.row]["body"] as NSString
        cell.likesQty!.text = self.learntItems[indexPath.row]["likes"] as NSString
        cell.commentsQty!.text = self.learntItems[indexPath.row]["comments"] as NSString
        cell.source!.text = self.learntItems[indexPath.row]["source"] as NSString
        cell.bodyBackground!.image = UIImage(named: self.learntItems[indexPath.row]["backgroundImage"] as NSString)
        if (self.learntItems[indexPath.row]["userLiked"] as String == "true"){
            cell.userLiked = true
        }
        
        if (cell.userLiked) {
            cell.likesBtn.image = UIImage(named: "like_filled.png")
        }
        
        let gotoProfileImageTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
        let gotoProfileTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoProfile:")
        let bodyTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "gotoBody:")
        let likesTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "likeIt:")
        let likesQtyTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "likeIt:")
        let commentTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "addComment:")
        
        cell.profileIcon!.addGestureRecognizer(gotoProfileImageTap)
        cell.username!.addGestureRecognizer(gotoProfileTap)
        cell.body!.addGestureRecognizer(bodyTap)
        cell.likesBtn.addGestureRecognizer(likesTap)
        cell.likesQty.addGestureRecognizer(likesQtyTap)
        cell.commentsBtn.addGestureRecognizer(commentTap)
        
        return cell
    }
    
    
    func gotoProfile(recognizer: UIGestureRecognizer){
        
        
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
            
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileViewController") as ProfileViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func gotoBody(recognizer: UIGestureRecognizer){
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
            let username = self.learntItems[indexPath.row]["username"] as NSString
            let profileIcon = self.learntItems[indexPath.row]["icon"] as NSString
            let body = self.learntItems[indexPath.row]["body"] as NSString
            let source = self.learntItems[indexPath.row]["source"] as NSString
            let bodyBackground = self.learntItems[indexPath.row]["backgroundImage"] as NSString
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BodyViewController") as BodyViewController
            vc.usernameData = username
            vc.profileIconData = profileIcon
            vc.bodyTextData = body
            vc.sourceData = source
            vc.bodyBackgroundData = bodyBackground
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
    }
    
    func likeIt(recognizer: UIGestureRecognizer){
        var point = recognizer.locationInView(self.tableView)
        if let indexPath = self.tableView.indexPathForRowAtPoint(point)
        {
            var likesQty = self.learntItems[indexPath.row]["likes"] as String
            var cell = self.tableView.cellForRowAtIndexPath(indexPath) as LearntTableViewCell
            
            if (cell.userLiked){
                var newLikesQty: Int = likesQty.toInt()! - 1
                
                //  self.learntItems[indexPath.row]["likes"] = "12"
                
                
                cell.likesQty!.text = String(newLikesQty)
                cell.userLiked = false
                cell.likesBtn.image = UIImage(named: "like_hollow.png")
            }else{
                var newLikesQty: Int = likesQty.toInt()! + 1
                cell.likesQty!.text = String(newLikesQty)
                cell.userLiked = true
                cell.likesBtn.image = UIImage(named: "like_filled.png")
            }
            
            
        }
        
        
    }
    
    func addComment(recognizer: UIGestureRecognizer){
        let cell = recognizer.view as LearntTableViewCell
        println("comment it: \(cell.commentsQty!.text)")
    }
    
    func addToFavorites(recognizer: UIGestureRecognizer){
        let cell = recognizer.view as LearntTableViewCell
        println("favor it: \(cell.favoriteQty!.text)")
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
