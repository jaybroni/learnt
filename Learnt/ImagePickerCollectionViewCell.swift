//
//  ImagePickerCollectionViewCell.swift
//  Learnt
//
//  Created by Jay on 3/2/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class ImagePickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    var data: JSON?{
        didSet{
            self.setupData()
        }
    }
    
    func setupData(){
        //self.image.image = UIImage(named: //)
    }
}
