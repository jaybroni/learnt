//
//  LoginViewController.swift
//  Learnt
//
//  Created by jay on 2/10/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {

    @IBOutlet weak var usernameView: UIView!

    @IBOutlet weak var passwordView: UIView!
    
    var keyboardShowing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        usernameView.layer.borderColor = UIColor.lightGrayColor().CGColor
        usernameView.layer.borderWidth = 1
        usernameView.layer.cornerRadius = 5.0
        
        passwordView.layer.borderColor = UIColor.lightGrayColor().CGColor
        passwordView.layer.borderWidth = 1
        passwordView.layer.cornerRadius = 5.0
        
        
        var tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tap)

            
        
    }

    func dismissKeyboard(){
        self.view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    @IBAction func backToLoginChooser(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func login(sender: AnyObject) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let tabBarController = mainStoryboard.instantiateViewControllerWithIdentifier("MainView") as? TabBarController {
            presentViewController(tabBarController, animated: true, completion: nil)
        }
        
        
           }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
