//
//  LearntTableViewCell.swift
//  Learnt
//
//  Created by jay on 2/13/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class LearntTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileIcon: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var body: UILabel!
    
    @IBOutlet weak var source: UILabel!
    
    @IBOutlet weak var likesQty: UILabel!
    @IBOutlet weak var likesBtn: UIImageView!
    
    @IBOutlet weak var commentsQty: UILabel!
    
    @IBOutlet weak var commentsBtn: UIImageView!
    @IBOutlet weak var favoriteBtn: UIImageView!
    @IBOutlet weak var favoriteQty: UILabel!
    @IBOutlet weak var bodyBackground: UIImageView!
    
    var userLiked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        body.sizeToFit()
        body.adjustsFontSizeToFitWidth = true
        body.baselineAdjustment = UIBaselineAdjustment.AlignBaselines
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
        
    }
    
    
}
