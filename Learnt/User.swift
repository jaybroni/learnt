//
//  User.swift
//  Learnt
//
//  Created by jay on 2/8/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class User: NSObject {
    var userId: Int?
    var firstName: String?
    var lastName: String?
    var email: String?
    var username: String?
}
