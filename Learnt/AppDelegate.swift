//
//  AppDelegate.swift
//  Learnt
//
//  Created by jay on 2/8/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
             // Override point for customization after application launch.
        
        var ref = Firebase(url:"https://learnt.firebaseio.com/")
        
        var dataJson = [[
            "username":"jaybroni",
            "icon":"Tiger-Woods_7.jpg",
            "body":"I just realized that my life long dream was to be a pilot and not a fighter",
            "source":"wikipedia.com",
            "likes": "25",
            "comments": "30",
            "favorites": "10",
            "postDate":"1h",
            "backgroundImage":"image4.jpg",
            "userLiked":"false"
            ],[
                "username":"joya",
                "icon":"Tiger-Woods_7.jpg",
                "body":"I just realized that my life long dream was to be a pilot and not a fighter",
                "source":"wikipedia.com",
                "likes": "25",
                "comments": "30",
                "favorites": "10",
                "postDate":"1h",
                "backgroundImage":"image4.jpg",
                "userLiked":"false"
            ]]
        
        var postRef = ref.childByAppendingPath("posts")
        var post1Ref = postRef.childByAutoId()
        //post1Ref.setValue(dataJson)
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

