//
//  ComposeViewController.swift
//  Learnt
//
//  Created by jay on 2/10/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class ComposeViewController: UIViewController,UITextViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var bodyTextView: UITextView!
    let placeholderText = "What did you learn?"
    var keyboardShowing = false
    
    //@IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var bodyText: UITextView!
    @IBOutlet weak var editTextView: UIView!
    @IBOutlet weak var editSourceView: UIView!
    @IBOutlet weak var editBackgroundView: UIView!
    
    @IBOutlet weak var canvasView: UIView!
    
    @IBOutlet weak var editSource: UIView!
    
    @IBOutlet weak var editSourceButton: UIView!
    @IBOutlet weak var sourceTextView: UITextView!
    
    
    @IBOutlet weak var editBackgroundSection: UIView!
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
    @IBOutlet weak var imageCollection: UICollectionView!
    
    var data: [JSON] = []
    
    @IBAction func cancelCompose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func finish(sender: AnyObject) {
        println(self.backgroundImage.frame.origin)
        println(self.backgroundImage.image?.scale)
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       var myRootRef = Firebase(url:"https://learnt.firebaseio.com/")
        myRootRef.observeEventType(.Value, withBlock: {
            snapshot in
            println("\(snapshot.key) -> \(snapshot.value)")
        })
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        
      //  self.bodyText.sizeToFit()
       // self.bodyText.layoutIfNeeded()
//        self.bodyText.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.New, context: nil)
        self.bodyText.layer.borderWidth = 3
        self.bodyText.layer.cornerRadius = 5
//        self.bodyText.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.bodyText.delegate = self
        self.bodyText.layer.borderColor = (UIColor(patternImage: UIImage(named: "dot")!)).CGColor
        
        self.bodyText.text = placeholderText
        self.bodyText.textColor = UIColorFromRGB(0xEEEEEE)
        let placeholderFont = UIFont(name: "Avenir", size: 19.0)!
//        self.sourceTextField.attributedPlaceholder = NSAttributedString(string: "Source (optional)", attributes: [NSForegroundColorAttributeName: UIColorFromRGB(0xDDDDDD), NSFontAttributeName: placeholderFont])
        let resignFirstResponder = UITapGestureRecognizer(target: self, action: "resignResponder")
        self.view.addGestureRecognizer(resignFirstResponder)
        
        
        // Do any additional setup after loading the view.
        let editTextTap = UITapGestureRecognizer(target: self, action: "editTextTap:")
        let editSourceTap = UITapGestureRecognizer(target: self, action: "editSourceTap:")
        let editBackgroundTap = UITapGestureRecognizer(target: self, action: "editBackgroundTap:")
        
        self.editTextView.addGestureRecognizer(editTextTap)
        self.editSourceButton.addGestureRecognizer(editSourceTap)
        self.editBackgroundView.addGestureRecognizer(editBackgroundTap)
        
        
        self.editSource.hidden = true
       self.imageCollection.delegate = self
        self.imageCollection.dataSource = self
        self.editBackgroundSection.hidden = true
        

    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    func editTextTap(r: UIGestureRecognizer){
        self.hideElements()
        self.bodyText.layer.borderWidth = 3
        self.bodyText.editable = true
        self.bodyText.hidden = false
        
    }
    
    func editSourceTap(r: UIGestureRecognizer){
        self.hideElements()
   
        self.editSource.hidden = false
        self.sourceTextView.hidden = false
        self.sourceTextView.layer.borderWidth = 1
        self.sourceTextView.layer.borderColor = UIColorFromRGB(0xAAAAAA).CGColor
    }
    
    func editBackgroundTap(r: UIGestureRecognizer){
        self.hideElements()
        self.editBackgroundSection.hidden = false
        
        request(.GET, "https://www.googleapis.com/customsearch/v1?q=golf&cx=005560496539624188291%3Akjnukb0cy6u&fileType=png%2C+jpg&rights=cc_sharealike&filter=1&safe=high&searchType=image&key=AIzaSyDcM06HVQpaVRSUaegreY8CMH_PZBEwDDY&&fields=items(image/thumbnailLink,link)&imgSize=medium").responseJSON { (request, response, json, error) in
            if json != nil {
                
                var jsonObj = JSON(json!)
                if let data = jsonObj["items"].arrayValue as [JSON]?{
                    self.data = data
                    println(self.data.count)
                    self.imageCollection.reloadData()
                    
                }
                
            }
        }
    }
    
    func hideElements(){
        self.bodyText.layer.borderWidth = 0
       
        self.bodyText.hidden = true
        self.editSource.hidden = true
        self.editBackgroundSection.hidden = true
        
    }

    override func viewWillAppear(animated: Bool) {


    }
    
    func resignResponder(){
        self.bodyText.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButton(sender: AnyObject) {
        
        // TODO: parse URL to just display the TLD ie. Facebook.com, slate.com, wikipedia.com
        
        // after successful save, pop view controller back
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    @IBAction func cancelButton(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        var textView: UITextView = object as UITextView
        var topoffset: CGFloat = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2.0
        topoffset = (topoffset < 0.0 ? 0.0 : topoffset)
        textView.contentOffset = CGPointMake(0, -topoffset)
        println("observeValueForKeyPath")
    }
    
    
    // MARK: - UITextView Delegate
 
    
    func textViewDidBeginEditing(textView: UITextView) {
        if (textView == bodyText){
            if (textView.text == placeholderText)
            {
                textView.text = ""
                textView.textColor = UIColor.whiteColor()
            }        }else
        {
            if bodyTextView.textColor == UIColor.lightGrayColor() {
                bodyTextView.text = nil
                bodyTextView.textColor = UIColor.blackColor()
            }
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView == bodyText){
            if(textView.text == "")
            {
                textView.text = placeholderText
                textView.textColor = UIColorFromRGB(0xEEEEEE)
            }

        }else{
            if bodyTextView.text.isEmpty {
                bodyTextView.text = placeholderText
                bodyTextView.textColor = UIColor.lightGrayColor()
            }
        }
    }
    
    //https://github.com/mattneub/Programming-iOS-Book-Examples/blob/master/bk2ch10p532textViewAndKeyboard/ch23p811selfSizingTextField/ViewController.swift
    func keyboardShow(n:NSNotification) {
        self.keyboardShowing = true
        let d = n.userInfo!
        var r = (d[UIKeyboardFrameEndUserInfoKey] as NSValue).CGRectValue()
       // self.view.frame = CGRectMake(0, -100, 320, 480);
    }
    
    func keyboardHide(n:NSNotification) {
        self.keyboardShowing = false
    // self.view.frame = CGRectMake(0, 0, 320, 480)
    }

    
func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
    
        return data.count
    }
 
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCell", forIndexPath: indexPath) as ImagePickerCollectionViewCell
       
        if let imageUrl = self.data[indexPath.row]["image"]["thumbnailLink"].string{
            
            let URL = NSURL(string: imageUrl)!
            let data = NSData(contentsOfURL: URL)!
            cell.image.image = UIImage(data: data)!
            
            let imageTap = UITapGestureRecognizer(target: self, action: "changeBackground:")
            cell.image.addGestureRecognizer(imageTap)
            
        }
        cell.data = self.data[indexPath.row]
//println(self.data[indexPath.row[]
        return cell
    }
 
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
 
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 3
    }

    func changeBackground(recognizer: UIGestureRecognizer)
    {
       var point = recognizer.locationInView(self.imageCollection)
        if let indexPath = self.imageCollection.indexPathForItemAtPoint(point)
        {
            
            let cell = self.imageCollection.cellForItemAtIndexPath(indexPath) as ImagePickerCollectionViewCell
            
            if let imageUrl = self.data[indexPath.row]["link"].string{
                
                let URL = NSURL(string: imageUrl)!
                let data = NSData(contentsOfURL: URL)!
                self.backgroundImage.image = UIImage(data: data)!
                
                self.editBackgroundSection.hidden = true
                self.bodyText.hidden = false
                
            }
            
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
