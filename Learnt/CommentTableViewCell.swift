//
//  CommentTableViewCell.swift
//  Learnt
//
//  Created by Jay on 2/21/15.
//  Copyright (c) 2015 Appsilog. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var timeAgo: UILabel!
    @IBOutlet weak var profileIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
